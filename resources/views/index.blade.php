<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
        <link rel="stylesheet" href="{{asset('css/sweetalert2.css')}}" />
        <link rel="stylesheet" href="{{asset('css/select2.css')}}" />
        <!-- Styles -->
        <style>
            .hidden
            {
                display: none;
            }
            .shown
            {
                display: block;
            }
        </style>
    </head>
    <body>
        <div class="form-group col-md-12">
            <h1>Test App</h1>
            <hr>
        </div>
        <div class="row col-md-12">
            <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />
            <div class="col-sm-3 border-right" style="background-color: #F3F3F3;min-height: 500px;padding-top: 20px">
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label class="control-label">Tags</label> 
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#tagModal">Add Tag</button>
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#viewTagModal">View List</button>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label class="control-label">Photos</label> 
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#uploadPhotoModal">Upload Photo</button>
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#viewPhotoModal">View List</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-9" style="padding-top: 20px;min-height: 500px;">
                <h3>Search Photo By Tag</h3>
                <div class="row form-group">
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="search_photo">
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-sm btn-success" id="search_button">Search</button>
                    </div>
                </div>
                <div class="form-group">
                    <table class="table" width="100%">
                        <tr style="background-color: #007BFF;color:#fff;" id="table_header" width="100%">
                            <th>Photo</th>
                            <th>Title</th>
                            <th>Tag/s</th>
                            <th>Date Created</th>
                        </tr>
                        <tbody id="search_photo_con"></tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Modal Upload Photo-->
        <div class="modal fade" id="uploadPhotoModal" tabindex="-1" role="dialog" aria-labelledby="uploadPhotoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Upload Photo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" id="image_canvas" name="image_canvas" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="control-label">Title</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="title" id="title" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="control-label">Upload Photo</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <img id="myImg" src="{{asset('images/BLANK_IMAGE.jpg')}}" alt="image" width="150px" height="150px" data-toggle="modal" data-target="#photo_modal"/>
                                        <input type="file" name="photo" id="photo" value="" onchange="displayImage(this);">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="control-label">Select Tag</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <select name="tag_arr[]" id="tag_arr" multiple style="width:100%;">
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save_photo">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal View Tag-->
        <div class="modal fade" id="viewPhotoModal" tabindex="-1" role="dialog" aria-labelledby="viewPhotoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Photo List</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tr style="background-color: #007BFF;color:#fff">
                                <th>Photo</th>
                                <th>Title</th>
                                <th>Tag/s</th>
                                <th>Date Created</th>
                            </tr>
                            <tbody id="photo_con">
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Add Tag-->
        <div class="modal fade" id="tagModal" tabindex="-1" role="dialog" aria-labelledby="tagModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Tag</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="control-label">Tag Name</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tag_name" id="tag_name" value="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="save_tag" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal View Tag-->
        <div class="modal fade" id="viewTagModal" tabindex="-1" role="dialog" aria-labelledby="viewTagModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tag List</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tr style="background-color: #007BFF;color:#fff">
                                <th>Tag Name</th>
                                <th>Date Created</th>
                            </tr>
                            <tbody id="tag_con">
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/select2.js')}}"></script>
<script type="text/javascript">
    $(function(){

        $("#search_button").click(function(){
            var search = $("#search_photo").val();

            if(search)
            {
                $.ajax(
                {
                    url:'{{{ URL::to("photo/search") }}}',
                    type:'get',
                    data:
                    {
                        'search': search,
                    },
                    async:false
                }).done(function(data){

                    $("#search_photo_con").empty();
                    if(data[0] == "")
                    {
                        $("#search_photo_con").append('<tr><td colspan="4" align="center"><h3>No data found</h3></td>'
                            +'</tr>');
                    }

                    $.map(data[0],function(item){
                        $("#search_photo_con").append('<tr>'
                            +'<td><img width="200px" src="'+item.photo_url+'"></td>'
                            +'<td>'+item.title+'</td>'
                            +'<td id="searc_tag_data_'+item.id+'"></td>'
                            +'<td>'+item.created_at+'</td></tr>');

                        var tags = data[1];
                        $.map(tags[item.id],function(item1){
                            $("#searc_tag_data_"+item.id).append(' #'+item1);
                        });
                    });
                });
            }
            else
            {
                swal("Please enter a tag name.");
            }
        });

        $("#tag_arr").select2();

        $("#save_tag").click(function(){
            $.ajax(
            {
                url:'{{{ URL::to("tag/save") }}}',
                type:'post',
                data:
                {
                    '_token': $('input[name=_token]').val(),
                    'tag_name': $("#tag_name").val(),
                },
                async:false,
                error: function(data){
                    swal("Error","Please enter tag name or tag name already exist!");
                }
            }).done(function(data){
                swal("Added Successfully!");
                $("#tagModal").modal('hide');
            });
        });

        $("#save_photo").click(function(){

            var tag_arr = $("#tag_arr").val();
            $.ajax(
            {
                url:'{{{ URL::to("photo/upload") }}}',
                type:'post',
                data:
                {
                    '_token': $('input[name=_token]').val(),
                    'title': $("#title").val(),
                    'tag_arr': tag_arr,
                    'image_canvas':$("#image_canvas").val(),
                },
                async:false,
                error: function(data){
                    swal("Error","Please enter title,tag and upload a photo or photo title already exist!");
                }
            }).done(function(data){
                swal("Added Successfully!");
                $("#tag_arr").select2("val", "");
                $("#myImg").removeAttr('src');
                $("#myImg").attr("src","images/BLANK_IMAGE.jpg");
                $("#title").val("");
                $("#photo").val("");
                $("#uploadPhotoModal").modal('hide');
            });
        });

        $('#tagModal').on('shown.bs.modal', function() {
            $("#tag_name").val("");
        });

        $('#viewTagModal').on('shown.bs.modal', function() {

            $.ajax(
            {
                url:'{{{ URL::to("tag/get") }}}',
                type:'get',
                async:false
            }).done(function(data){

                $("#tag_con").empty();
                $.map(data,function(item){
                    $("#tag_con").append('<tr><td>'+item.name+'</td><td>'+item.created_at+'</td></tr>');
                });
            });
        });

        $('#viewPhotoModal').on('shown.bs.modal', function() {

            $.ajax(
            {
                url:'{{{ URL::to("photo/get") }}}',
                type:'get',
                async:false
            }).done(function(data){

                $("#photo_con").empty();
                $.map(data[0],function(item){
                    $("#photo_con").append('<tr>'
                        +'<td><img width="50px" src="'+item.photo_url+'"></td>'
                        +'<td>'+item.title+'</td>'
                        +'<td id="tag_data_'+item.id+'"></td>'
                        +'<td>'+item.created_at+'</td></tr>');

                    var tags = data[1];
                    $.map(tags[item.id],function(item1){
                        $("#tag_data_"+item.id).append(' #'+item1);
                    });
                });
            });
        });

        $('#uploadPhotoModal').on('shown.bs.modal', function() {
            $.ajax(
            {
                url:'{{{ URL::to("tag/get") }}}',
                type:'get',
                async:false
            }).done(function(data){

                $("#tag_arr").empty();
                $.map(data,function(item){
                    $("#tag_arr").append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            });
        });
    });

    function displayImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#myImg').attr('src', e.target.result);
                $('#image_canvas').val(e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
