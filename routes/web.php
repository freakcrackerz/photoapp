<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PhotoAppController@index');
Route::post('tag/save', 'PhotoAppController@saveTag');
Route::get('tag/get', 'PhotoAppController@getTag');
Route::post('photo/upload', 'PhotoAppController@uploadPhoto');
Route::get('photo/get', 'PhotoAppController@getPhoto');
Route::get('photo/search', 'PhotoAppController@searchPhoto');
