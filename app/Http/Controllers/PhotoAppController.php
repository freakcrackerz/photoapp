<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Photo;
use App\PhotoTag;
use App\Http\Requests\TagRequest;
use App\Http\Requests\PhotoRequest;
use Illuminate\Http\Request;
use Input;

class PhotoAppController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function saveTag(TagRequest $tag_request)
    {
    	$tag = new Tag();
    	$tag -> name = $tag_request -> tag_name;
    	$tag -> save();

    	return;
    }

    public function getTag()
    {
    	$tag_list = Tag::get();
    	
    	return response()->json($tag_list);
    }

    public function uploadPhoto(PhotoRequest $photo_request)
    {

    	if($photo_request -> title && $photo_request -> image_canvas && $photo_request -> tag_arr)
    	{
	        $photo = new Photo();
	    	$photo -> title = $photo_request -> title;

	    	$image_canvas = $photo_request -> image_canvas;

	    	if($image_canvas)
		    {
		        list($type, $image_canvas) = explode(';', $image_canvas);
		        list(, $image_canvas)      = explode(',', $image_canvas);
		        $image_canvas = base64_decode($image_canvas);
		    }
	    	
		    $value = $image_canvas;
		    if($value != "")
		    {
		          	$file = 'images/photos/' .ucwords($photo_request -> title).'.png';
		          	file_put_contents($file, $image_canvas);
		    }

	    	$photo -> photo_url = $file;
	    	$photo -> save();

	        foreach ($photo_request -> tag_arr as $tag) {
	        	$photo_tag = new PhotoTag();
	        	$photo_tag -> photo_id = $photo -> id;
	        	$photo_tag -> tag_id = $tag;
	        	$photo_tag -> save();
	        }
		    return;
	    }
    }

    public function getPhoto()
    {
    	$photo_list = Photo::get();

    	$photo_arr = [];

    	foreach ($photo_list as $photo)
    	{
    		$photo_tag_list = PhotoTag::join('tag','photo_tag.tag_id','=','tag.id')->where('photo_tag.photo_id',$photo -> id)->get();

    		foreach ($photo_tag_list as $photo_tag) {
    			$photo_arr[$photo -> id][$photo_tag->tag_id] = $photo_tag -> name;
    		}
    	}

    	$data[0] = $photo_list;
    	$data[1] = $photo_arr;

    	return response()->json($data);
    }

    public function searchPhoto()
    {
    	$search = Input::get('search');

    	$photo_list = PhotoTag::join('photo','photo_tag.photo_id','=','photo.id')
    		->join('tag','photo_tag.tag_id','=','tag.id')
    		// ->where('tag.name','LIKE','%'.$search.'%')
    		->where('tag.name','LIKE',$search)
    		->select('photo_tag.photo_id as id','photo.photo_url','photo.title','photo.created_at')
    		->groupBy('photo_tag.photo_id')
    		->get();

    	$photo_arr = [];

    	foreach ($photo_list as $photo)
    	{
    		$photo_tag_list = PhotoTag::join('tag','photo_tag.tag_id','=','tag.id')->where('photo_tag.photo_id',$photo -> id)->get();

    		foreach ($photo_tag_list as $photo_tag) {
    			$photo_arr[$photo -> id][$photo_tag->tag_id] = $photo_tag -> name;
    		}
    	}

    	$data[0] = $photo_list;
    	$data[1] = $photo_arr;

    	return response()->json($data);
    }

}
